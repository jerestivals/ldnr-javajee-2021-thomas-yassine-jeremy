/*
 * @author Jérémy
 *
 */
$(function () {
    dispos();
    empruntes();
//====== Fonctions pour faire des actions suite a la "validation de la saisie"
    /**
     * Enregistrer un livre
     * @author Jérémy
     */
    $("#enregistrerLivre").on('click', function () {

        let titreLivre = String($("#titreLivre").val());
        let anneeLivre = parseInt($("#anneeLivre").val(), 10);
        let editeurLivre = String($("#editeurLivre").val());
        let nomAuteurLivre = String($("#nomAuteurLivre").val());
        let prenomAuteurLivre = String($("#prenomAuteurLivre").val());
        /**
         * récupration des informations des champs du html
         */
        $.ajax({
            url: "/livre/creer",
            type: "POST",
            contentType: "application/json",
            data: JSON.stringify({
                "titre": titreLivre,
                "annee": anneeLivre,
                "nomEditeur": editeurLivre,
                "nomAuteur": nomAuteurLivre,
                "prenomAuteur": prenomAuteurLivre
            })
                    /**
                     * envoi a l'adresse visée un objet Json et attend une réponse
                     * @returns
                     * @param {type} livre
                     * @returns {jqXHR|String}  Livre via le Livre controlleur qui surveille cette adresse
                     */
        }).done(function (livre) {
            if (livre.idLivre !== undefined) {
                return   $.ajax({
                    url: "/livre/" + livre.idLivre,
                    type: "POST",
                    contentType: "application/json",
                    data: JSON.stringify({
                        "idLivre": livre.idLivre,
                        "titre": livre.titre,
                        "annee": livre.annee,
                        "nomEditeur": livre.nomEditeur,
                        "nomAuteur": livre.nomAuteur,
                        "prenomAuteur": livre.prenomAuteur
                    })


                });
            } else {
                return "Aucun échec mais rien a retourner";
            }
        })

                /**
                 *  * si la requete s'est bien passée stocke le livre qu'il a recu du controlleur dans une adresse de type /livre/id il retourne un livre
                 * sinon le retour sert juste apreciser que le resultat n'est pas une empty string donc l'action reussi mais il n'y a pas de requete a faire
                 * @param {type} data
                 * @returns {undefined} pas de retour
                 */
                .done(function (data) {
                    verifEmptyStringEtMetAJour(data);
                    /**
                     * vérifie qu'aucune exception n'a été levée et met a jours les listes
                     */
                }).fail(function () {
            actionEchouee(null);
        });
    });
});
/**
 * dans les cas si dessus affiche un popup la saisie a echouée
 */

/**
 * Créer un emprunt
 * @author Jérémy
 */
$("#validerCreerEmprunt").on('click', function () {
    let livreid = formatSelect($("#livreCreerSelectEmprunt").val());

    let nomUsagerCreerEmprunt = $("#nomUsagerCreerEmprunt").val();
    let dateEnCoursEmprunt = $("#dateEnCoursEmprunt").val();
    /**
     * récupration des informations des champs du html la date subissant un formattage et le livre id etant récupéré du contenu de la liste déroulante
     */
    $.ajax({
        url: "/emprunt/creer",
        type: "POST",
        contentType: "application/json",
        data: JSON.stringify({
            "idLivre": livreid,
            "nomEmprunteur": nomUsagerCreerEmprunt,
            "dateEnCours": dateEnCoursEmprunt
        })
                /**
                 * envoi a l'adresse visée un objet Json et attend une réponse
                 * @returns Emprunt via le Emprunt controlleur qui surveille cette adresse
                 * @param {Jquery} data
                 */
    }).done(function (data) {
        verifEmptyStringEtMetAJour(data);
        /**
         * vérifie qu'aucune exception n'a été levée et met a jours les listes
         */
    })
            .fail(function () {
                actionEchouee(null);
            });
    /**
     * dans le cas si dessus affiche un popup la saisie a echouée
     */
});
/**
 * Modifier un emprunt
 * @author Jérémy
 */
$("#validerModifierEmprunt").on('click', function () {
    let livreId = formatSelect($("#livreModifierSelectEmprunt").val());
    let dateRenduEmprunt = $("#dateRenduEmprunt").val();
    /**
     * récupration des informations des champs du html la date subissant un formattage et le livre id etant récupéré du contenu de la liste déroulante
     */
    $.ajax({
        url: "/emprunt/modifier",
        type: "POST",
        contentType: "application/json",
        data: JSON.stringify({
            "idLivre": livreId,
            "dateRendu": dateRenduEmprunt
        })
                /**
                 * envoi a l'adresse visée un objet Json et attend une réponse
                 * @returns Emprunt via le Emprunt controlleur qui surveille cette adresse
                 * @param {jQuery} data
                 */
    }).done(function (data) {
        verifEmptyStringEtMetAJour(data);
        /**
         * vérifie qu'aucune exception n'a été levée et met a jours les listes
         */
    }).fail(function () {
        actionEchouee(null);
    });
});
/**
 * dans le cas si dessus affiche un popup la saisie a echouée
 */
//========= Fonction de vérification
/**
 *
 * @param {type} data
 * @returns {undefined} Rien
 */
function verifEmptyStringEtMetAJour(data) {
    if (data.length === 0) {
        actionEchouee(null);
    } else {
        actionReussie(null);
        empruntes();
        dispos();
    }
}
// ============= Fonction de conversions ====================

/**
 * prend le contenu de la liste deroulante et retourne le premier nombre sur lequel elle tombe, donc l'id
 * @param {type} data
 * @returns Integer id
 */
function formatSelect(data) {

    return parseInt(data, 10);
}
//============= Fonctions d'envoi de message =================
/**
 * deux fonctions qui créé des popup de messages et qui peuvent être personnalisée
 * @param data un message texte a afficher ou null (toujours null dans ce code)
 * @author Jérémy
 */
function actionReussie(data) {
    if (data === null) {
        data = "La saisie est valide";
    }
    window.alert(data);
}
function actionEchouee(data) {
    if (data === null) {
        data = "La saisie à échouée";
    }
    window.alert(data);
}

//====== fonctions qui en récupérant une liste la formate pour l'afficher
/**
 * deux fonction ont un role similaire mais un affichage différent l'un dans une liste deroulante l'autre dans une table
 *
 * @param {type} data l'objet liste
 * @param {type} idDuControle defini dans le html (la cible de la modification
 *
 * @author Thomas
 * @author Jérémy
 */
function creerListeDeroulanteLivre(data, idDuControle) {
    let liste = "";
    for (let i = 0; i < data.length; i++) {

        if (typeof (data[i].idLivre) === 'number') {
            liste += "<option>" + data[i].idLivre + " - Titre : " + data[i].titre + ", Nom d'auteur : " + data[i].nomAuteur + ", Editeur : " + data[i].nomEditeur + "</option>";
        } else {
            liste += "<option>" + data[i].idEmprunt + " - Livre n°" + data[i].livre.idLivre + " - Emprunté par : " + data[i].nomEmprunteur + " - Titre : " + data[i].livre.titre + ", Nom d'auteur : " + data[i].livre.nomAuteur + ", Editeur : " + data[i].livre.nomEditeur + "</option>";
        }
    }
    $(idDuControle).html(liste);
}


function creerTableEmprunt(data, idDuControle) {
    let liste = "";
    for (let i = 0; i < data.length; i++) {
        let formatDateRetour = data[i].dateEnCours.split("-");

        liste += "<tr><td>" + " " + data[i].livre.idLivre + "</td><td>" + formatDateRetour[2] + "/" + formatDateRetour[1] + "/" + formatDateRetour[0] + "</td><td>" + data[i].nomEmprunteur + "</td></tr>";
    }
    $(idDuControle).html(liste);
}

//========== fonctions de mise a jours des listes
/**
 *
 * mise a jour des livres disponibles sur l'adresse visée suite au depot de l'emprunt controlleur ou du livre controleur a cette adresse
 * @author Jérémy
 */
function dispos() {
    $.ajax({
        url: "/livre/dispos",
        type: "POST",
        data: null
    }).done(function (liste) {
        creerListeDeroulanteLivre(liste, "#livreCreerSelectEmprunt");
    }).fail(function () {
        actionEchouee(null);
    });
}

/**
 *
 * mise a jour des livres disponibles sur l'adresse visée suite au depot de l'emprunt controlleur  a cette adresse
 * @author Jérémy
 */

function empruntes() {
    $.ajax({
        url: "/livre/empruntes",
        type: "POST",
        data: null
    }).done(function (liste) {
        creerListeDeroulanteLivre(liste, "#livreModifierSelectEmprunt");
        creerTableEmprunt(liste, "#tableLivresEmprunt");
    }).fail(function () {
        actionEchouee(null);
    });
}


