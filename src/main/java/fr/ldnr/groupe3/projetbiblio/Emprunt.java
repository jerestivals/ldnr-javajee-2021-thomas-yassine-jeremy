package fr.ldnr.groupe3.projetbiblio;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * @author Jérémy
 * @author Thomas
 */
// Création de la table emprunts
@Entity
@Table(name = "emprunts")
public class Emprunt implements Serializable {

//    Création variables
    private Livre livre;
    private String nomEmprunteur;
    private Date dateEnCours;
    private Date dateRendu;
    private int idEmprunt = 0;

    // construction nul
    public Emprunt() {
        this(null, null, null, null);
    }

    // constructeur complet
    public Emprunt(Livre livre, String nomEmprunteur, Date dateEnCours, Date dateRendu) {
        this.livre = livre;
        this.nomEmprunteur = nomEmprunteur;
        this.dateEnCours = dateEnCours;
        this.dateRendu = dateRendu;
        this.idEmprunt = 0;
    }

    // définition de l'ID pour la table
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idEmprunt")
    public int getIdEmprunt() {
        return idEmprunt;
    }

    public void setIdEmprunt(int idEmprunt) {
        this.idEmprunt = idEmprunt;
    }

    @OneToOne(cascade = CascadeType.ALL)
    public Livre getLivre() {
        return livre;
    }

    public void setLivre(Livre livre) {
        this.livre = livre;
    }

    @Column(length = 100, nullable = false)
    public String getNomEmprunteur() {
        return nomEmprunteur;
    }

    public void setNomEmprunteur(String nomEmprunteur) {
        this.nomEmprunteur = nomEmprunteur;
    }

    @Column(nullable = false)
    @Temporal(TemporalType.DATE)
    public Date getDateEnCours() {
        return dateEnCours;
    }

    public void setDateEnCours(Date dateEnCours) {
        this.dateEnCours = dateEnCours;
    }

    @Column(nullable = true)
    @Temporal(TemporalType.DATE)
    public Date getDateRendu() {
        return dateRendu;
    }

    public void setDateRendu(Date dateRendu) {
        this.dateRendu = dateRendu;
    }

    @Override
    public String toString() {
        return " Emprunt n°" + idEmprunt + " nomEmprunteur =" + nomEmprunteur + " livre=" + livre;
    }
}
