package fr.ldnr.groupe3.projetbiblio;

import com.fasterxml.jackson.databind.node.ObjectNode;
import java.util.Date;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Jérémy
 * @author Thomas
 */
@RestController
public class EmpruntControleur {

    public static final Logger logger = LoggerFactory.getLogger(EmpruntControleur.class);

    private SessionFactory sessionFactory;

    @Autowired
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    /**
     * @author Jérémy
     * @param object
     * @return Emprunt emprunt
     */
    @RequestMapping(path = "/emprunt/creer", method = RequestMethod.POST)
    public Emprunt envoi(@RequestBody ObjectNode object) {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        try {
            int idLivre = object.get("idLivre").asInt();
            String nomEmprunteur = object.get("nomEmprunteur").asText();
            String dateTexte = object.get("dateEnCours").asText();
            /**
             * recuperer les objets sur la page créée
             */
            if (Validateurs.verifString(nomEmprunteur, Validateurs.MAX_NOMEMPRUNTEUR) == false) {
                throw new Exception();
            }
            /**
             * vérifie que le champs nom emprunteur est correct
             */
            Livre livre = (Livre) session.createQuery("from Livre where idLivre = :id").setParameter("id", idLivre).getSingleResult();
            /**
             * créer le livre en fonction de ce qui est dans la base
             */
            if (Validateurs.verifierLaDate(dateTexte) == false) {
                throw new Exception();
            }
            String[] tabDate = dateTexte.split("/");
            int annee = Integer.parseInt(tabDate[2]);
            int mois = Integer.parseInt(tabDate[1]);
            int jour = Integer.parseInt(tabDate[0]);
            if (Validateurs.verifierAnnee(Validateurs.MIN_ANNEEEMPRUNT, Validateurs.MAX_ANNEE, annee) == false) {
                throw new Exception();
            }
            Date dateEnCours = new Date(annee - 1900, mois - 1, jour);
            /**
             * verifier qu'il s'agit d'une date valide créer la date en fonction
             * des élément récupérés les additions et soustractions prennent en
             * comptent des spécificité de l'objet date - qui compte comme 0
             * l'année 1900 - qui compte pour 0 le mois de janvier
             */
            Emprunt creeEmprunt = new Emprunt(livre, nomEmprunteur, dateEnCours, null);
            creeEmprunt.getLivre().setEstEmprunte(true);
            /**
             * initialisation du nouvel emprunt suivant les champs récupéré au
             * dessus en ajoutant le booléen correct
             */
            session.save(creeEmprunt);
            return creeEmprunt;
        } catch (Exception exception) {
            return null;
            /**
             * si une exception est attrapée null est renvoyé sur la page web et
             * Javascript se servira de ceci pour afficher que la saisie a
             * échouée
             */
        } finally {
            tx.commit();
            session.close();
        }
    }

    /**
     * @author Jérémy récupère l'objet généré par le javascript est met a jour
     * l'emprunt visé
     *
     * @param modifEmprunt objet créé par le javascript qui doit etre traité
     * @return Emprunt empruntmodifie ou null
     */
    @RequestMapping(path = "/emprunt/modifier", method = RequestMethod.POST)
    public Emprunt modifierEmprunt(@RequestBody ObjectNode modifEmprunt) {
        int idEmprunt = modifEmprunt.get("idLivre").asInt();
        String dateTexte = modifEmprunt.get("dateRendu").asText();
        /**
         * recuperer les objets sur la page créée
         */
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        try {
            if (Validateurs.verifierLaDate(dateTexte) == false) {
                throw new Exception();
            }
            String[] tabDate = dateTexte.split("/");
            int annee = Integer.parseInt(tabDate[2]);
            int mois = Integer.parseInt(tabDate[1]);
            int jour = Integer.parseInt(tabDate[0]);

            if (Validateurs.verifierAnnee(Validateurs.MIN_ANNEEEMPRUNT, Validateurs.MAX_ANNEE, annee) == false) {
                throw new Exception();
            }
            Date dateRendu = new Date(annee - 1900, mois - 1, jour);
            /**
             * verifier qu'il s'agit d'une date valide créer la date en fonction
             * des élément récupérés les additions et soustractions prennent en
             * comptent des spécificité de l'objet date - qui compte comme 0
             * l'année 1900 - qui compte pour 0 le mois de janvier
             */
            Emprunt empruntmodifie = (Emprunt) session.createQuery("from Emprunt where idEmprunt = :id").setParameter("id", idEmprunt).getSingleResult();
            /**
             * récupérer le bon emprunt et créer l'objet
             */
            if (empruntmodifie.getDateEnCours() != null) {
                if (empruntmodifie.getDateEnCours().compareTo(dateRendu) > 0) {
                    throw new Exception();
                }
            } else {
                throw new Exception();
            }
            /**
             * vérifier que la date de rendu est au moins égale a la date
             * d'emprunt (terme métier de dateEnCours)
             */
            empruntmodifie.setDateRendu(dateRendu);
            empruntmodifie.getLivre().setEstEmprunte(false);
            /**
             * ajouter a l'objet une date de rendu et de le classer dans les
             * livres emprunté en modifiant la valeur de boleéen
             */

            session.save(empruntmodifie);
            return empruntmodifie;
        } catch (Exception exception) {
            return null;
            /**
             * si une exception est attrapée null est renvoyé sur la page web et
             * Javascript se servira de ceci pour afficher que la saisie a
             * échouée
             */
        } finally {
            tx.commit();
            session.close();
        }

    }

    /**
     *
     * réalise une requete sur la table livre pour récupérer la liste des livres
     * dispos
     *
     * @author Jérémy
     * @return Emprunt List listelivre ou null
     *
     */
    @RequestMapping(path = "/livre/empruntes", method = RequestMethod.POST)
    public List creerEmpruntEnCoursListe() {
        try (Session session = sessionFactory.openSession()) {
            String livresEmpruntes = "FROM Emprunt  where dateRendu IS null and dateEnCours IS NOT null order by dateEnCours ASC";
            List<Object> listeLivre = session.createQuery(livresEmpruntes)
                    .getResultList();
            /**
             * récupérer et créer la liste qui contient les livre dont la date
             * de rendu est nul(non rendu) mais dont la date d'emprunt n'est pas
             * null pour exclure les livres nouvellement créé
             */
            return listeLivre;
        } catch (Exception exception) {
            return null;
            /**
             * si une exception est attrapée null est renvoyé sur la page web et
             * Javascript se servira de ceci pour afficher que la saisie a
             * échouée
             */
        }
    }

    /**
     * @author Jérémy
     * @return listeLivres ou null
     */
    @RequestMapping(path = "/livre/dispos", method = RequestMethod.POST)
    public List creerLivresDispoListe() {
        try (Session session = sessionFactory.openSession()) {
            String livresListe = "from Livre where estEmprunte = false";
            List<Livre> listeLivre = (List) session.createQuery(livresListe).getResultList();
            /**
             * récupérer et créer la liste qui contient les livre dont le boleen
             * est emprunté est sur false donc tout les livre dont l'emprunt
             * n'est pas en cours soit qu'il vienne juste d'etre créé soit
             * qu'ils ont été rendu
             */
            return listeLivre;
        } catch (Exception exception) {
            return null;
            /**
             * si une exception est attrapée null est renvoyé sur la page web et
             * Javascript se servira de ceci pour afficher que la saisie a
             * échouée
             */
        }
    }
}
