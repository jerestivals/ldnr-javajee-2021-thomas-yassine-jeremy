/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.ldnr.groupe3.projetbiblio;

/**
 * Classe chargée de vérifier les saisie
 *
 * @author Jérémy
 */
public class Validateurs {

    public final static int MAX_TITRE = 100;
    public final static int MAX_NOMAUTEUR = 80;
    public final static int MAX_NOMEDITEUR = 100;
    public final static int MAX_PRENOMAUTEUR = 80;

    public final static int MAX_NOMEMPRUNTEUR = 100;

    public final static int MAX_ANNEE = 2099;
    public final static int MIN_ANNEEEMPRUNT = 2000;
    public final static int MIN_ANNEELIVRE = -5000;

    /**
     * Méthode pour eviter les champs vide et les injection SQL le mélange
     * francais anglais etant peu probable
     *
     * @param string
     * @param limiteDecaractere
     * @return String "verif ok" ou ""
     *
     * @author Jérémy
     */
    public static boolean verifString(String string, int limiteDecaractere) {

        try {
            if (string.equals("") | string.toLowerCase().contains(" from emprunt") | string.toLowerCase().contains(" from livre")) {
                throw new Exception();
            }
            /**
             * vérifie que les saisies interdites sont bien absentes a savoir
             * les saisie vides et les saisie présentant un risques au niveau de
             * l'injection de SQL
             */
            if (string.length() > limiteDecaractere) {
                throw new Exception();
            }
            /**
             * vérifie que la saisie n'outrepasse pas les limites prévues pour
             * les tables de données
             */
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * @author Jérémy
     * @param date
     * @return true ou false
     */
    public static boolean verifierLaDate(String date) {
        int nombreJoursParMois = 0;
        try {
            String[] tabDate = date.split("/");
            int annee = Integer.parseInt(tabDate[2]);
            int mois = Integer.parseInt(tabDate[1]);
            int jour = Integer.parseInt(tabDate[0]);
            /**
             * essaye de parser les données recues
             */
            if (mois == 1 | mois == 3 | mois == 5 | mois == 7 | mois == 8 | mois == 10 | mois == 12) {
                nombreJoursParMois = 31;
            } else if (mois == 4 | mois == 6 | mois == 9 | mois == 11) {
                nombreJoursParMois = 30;
            } else if (mois == 2) {
                if (annee % 100 == 0) {
                    nombreJoursParMois = 28;
                } else {
                    if (annee % 4 == 0) {
                        nombreJoursParMois = 29;
                    } else {
                        nombreJoursParMois = 28;
                    }
                }
                if (annee % 400 == 0) {
                    nombreJoursParMois = 29;
                }
                /**
                 * détermine le nombre de jour du mois choisi
                 */
            } else {
                throw new Exception();
            }
            /**
             * leve une exception si le mois est incorrect
             */

            if (jour > nombreJoursParMois | jour < 1) {
                throw new Exception();
            }
            /**
             * leve une exception si le jour est incorrect
             */
            /**
             * note : l'année est vérifiée directement dans les deux
             * controlleurs
             */
            return true;
        } catch (Exception e) {
            return false;
            /**
             * renvoi false si une exception est levée
             */
        }

    }

    public static boolean verifierAnnee(int minimum, int maximum, int valeur) {
        try {
            if (valeur > maximum) {
                throw new Exception();
            }
            if (valeur < minimum) {
                throw new Exception();
            }
            return true;
            /**
             * vérifie que la date est bien dans les bornes demandées;
             */
        } catch (Exception e) {
            return false;
            /**
             * renvoi false si une exception est levée
             */
        }
    }

//    @RequestMapping("/error")
//    @ResponseBody
//    public static String genererPageErreur() {
//        return "<html>"
//                + "<head>"
//                + "<title>Page indisponible(erreur 404)</title>"
//                + "<meta charset=\"UTF-8\">\n"
//                + "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n"
//                + "<link rel=\"icon\" type=\"image/x-icon\" href=\"favicon.ico\" />\n"
//                + "<script src=\"https://code.jquery.com/jquery-3.6.0.min.js\"\n"
//                + "                integrity=\"sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=\"\n"
//                + "        crossorigin=\"anonymous\" defer></script>\n"
//                + "        <script src=\"codeJavascript.js\" defer></script>\n"
//                + "        <link rel=\"stylesheet\" href=\"/style.css\"/>"
//                + "</head>"
//                + "<body><h1>Oups...(404)</h1>"
//                + "<p>La page que vous vouliez voir n'est pas disponible<p></body></html>";
//    }
//    @Override
//    public String getErrorPath() {
//        return null;
//    }
    /**
     * tentative de modifier la page d'erreur
     */
}
