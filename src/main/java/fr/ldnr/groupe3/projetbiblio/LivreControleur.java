package fr.ldnr.groupe3.projetbiblio;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Thomas
 * @author Jérémy
 */
@RestController
public class LivreControleur {

    public static final Logger logger = LoggerFactory.getLogger(LivreControleur.class);

    private SessionFactory sessionFactory;

    public LivreControleur() {

    }

    @Autowired
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    /**
     * @author Jérémy
     * @param livre
     * @return livre ou null
     */
    @RequestMapping(method = RequestMethod.POST, path = "/livre/creer")
    public Livre insertionlivre(@RequestBody Livre livre) {

        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        try {
            if (Validateurs.verifString(livre.getTitre(), Validateurs.MAX_TITRE) == false) {
                throw new Exception();
            }
            if (Validateurs.verifString(livre.getNomAuteur(), Validateurs.MAX_NOMAUTEUR) == false) {
                throw new Exception();
            }
            if (Validateurs.verifString(livre.getNomEditeur(), Validateurs.MAX_NOMEDITEUR) == false) {
                throw new Exception();
            }
            if (Validateurs.verifString(livre.getPrenomAuteur(), Validateurs.MAX_PRENOMAUTEUR) == false) {
                throw new Exception();
            }
            if (Validateurs.verifierAnnee(Validateurs.MIN_ANNEELIVRE, Validateurs.MAX_ANNEE, livre.getAnnee()) == false) {
                throw new Exception();
            }
            /**
             * vérification que les champs du formulaire sont correct,
             */
            session.save(livre);
            return livre;
        } catch (Exception exception) {
            return null;
            /**
             * si une exception est attrapée null est renvoyé sur la page web et
             * Javascript se servira de ceci pour afficher que la saisie a
             * échouée
             */
        } finally {
            tx.commit();
            session.close();
        }

    }

    /**
     * @author Jérémy création d'une page personnalisée pour chaque livre
     * @param id du livre
     * @return String d'une page html ou null ce qui affiche une page noire
     *
     */
    @RequestMapping(path = "/livre/{id}")
    public String obtenirLivre(@PathVariable int id) {

        try (Session session = sessionFactory.openSession()) {
            String livreparID = "from Livre where idLivre = :id";
            Livre objet = (Livre) session.createQuery(livreparID).setParameter("id", id).uniqueResult();
            return "<html>"
                    + "<head>"
                    + "<title>Page du livre n°" + objet.getIdLivre() + "</title>"
                    + "<meta charset=\"UTF-8\">\n"
                    + "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n"
                    + "<link rel=\"icon\" type=\"image/x-icon\" href=\"favicon.ico\" />\n"
                    + "<script src=\"https://code.jquery.com/jquery-3.6.0.min.js\"\n"
                    + "                integrity=\"sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=\"\n"
                    + "        crossorigin=\"anonymous\" defer></script>\n"
                    + "        <script src=\"../codeJavascript.js\" defer></script>\n"
                    + "        <link rel=\"stylesheet\" href=\"/style.css\"/>"
                    + "</head>"
                    + "<body><h1>" + objet.getTitre() + " (id n°" + objet.getIdLivre() + ")</h1>"
                    + "<p>Le livre n°" + objet.getIdLivre() + " a pour titre " + objet.getTitre() + ". Il a été écrit par " + objet.getPrenomAuteur() + " " + objet.getNomAuteur() + ". L'exemplaire que vous avez entre les mains a été publié chez " + objet.getNomEditeur() + " en " + objet.getAnnee() + "<p></body></html>";
        } catch (Exception e) {
            return "<html>"
                    + "<head>"
                    + "<title>Page du livre n°" + id + "</title>"
                    + "<meta charset=\"UTF-8\">\n"
                    + "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n"
                    + "<link rel=\"icon\" type=\"image/x-icon\" href=\"favicon.ico\" />\n"
                    + "<script src=\"https://code.jquery.com/jquery-3.6.0.min.js\"\n"
                    + "                integrity=\"sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=\"\n"
                    + "        crossorigin=\"anonymous\" defer></script>\n"
                    + "        <script src=\"../codeJavascript.js\" defer></script>\n"
                    + "        <link rel=\"stylesheet\" href=\"/style.css\"/>"
                    + "</head>"
                    + "<body><h1>Ce livre n'existe pas</h1>"
                    + "<p>Le livre n'existe plus, rapprochez vous de votre bibliothécaire pour en apprendre davantage la dessus<p></body></html>";
            /**
             * si une exception est attrapée une page web personnalisée est
             * préparée pour envoi
             */
        }

    }

}
