package fr.ldnr.groupe3.projetbiblio;

import java.util.Date;
import java.util.Properties;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class HibernateConfiguration {

//    Création logger
    public static final Logger logger = LoggerFactory.getLogger(HibernateConfiguration.class);

    @Bean
    public static SessionFactory sessionFactory() {
// choix des options hibernate
        Properties options = new Properties();
        options.put("hibernate.dialect", "org.sqlite.hibernate.dialect.SQLiteDialect");
        options.put("hibernate.hbm2ddl.auto", "update");
        options.put("hibernate.show_sql", "false");
        options.put("hibernate.connection.driver_class", "org.sqlite.JDBC");
        options.put("hibernate.connection.url", "jdbc:sqlite:tablesBiblio.sqlite");
        SessionFactory factory = new org.hibernate.cfg.Configuration().
                addProperties(options)
                .addAnnotatedClass(Livre.class)
                .addAnnotatedClass(Emprunt.class)
                .addAnnotatedClass(Date.class)
                .buildSessionFactory();
        return factory;
    }
}
