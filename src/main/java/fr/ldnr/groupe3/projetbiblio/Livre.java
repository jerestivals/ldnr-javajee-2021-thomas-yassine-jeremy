package fr.ldnr.groupe3.projetbiblio;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author Jérémy
 * @author Thomas
 */
// création de la table
@Entity
@Table(name = "livres")
public class Livre implements Serializable {

    // création des variables
    private String titre;
    private int annee;
    private String nomEditeur;
    private String prenomAuteur;
    private String nomAuteur;
    private int idLivre;
    private boolean estEmprunte;

    // constructeur complet
    public Livre(String titre, int annee, String nomEditeur, String prenomAuteur, String nomAuteur) {
        this.titre = titre;
        this.annee = annee;
        this.nomEditeur = nomEditeur;
        this.prenomAuteur = prenomAuteur;
        this.nomAuteur = nomAuteur;
        this.idLivre = 0;
        this.estEmprunte = false;
    }

    // constructeur nul avec date définie
    public Livre() {
        this(null, 2021, null, null, null);
    }

    // définition de l'Id de la table
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idLivre")
    public int getIdLivre() {
        return idLivre;
    }

    public void setIdLivre(int idLivre) {
        this.idLivre = idLivre;
    }

    @Column(length = Validateurs.MAX_TITRE, nullable = false)
    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    @Column(nullable = false, name = "annee")
    public int getAnnee() {
        return annee;
    }

    public void setAnnee(int annee) {
        this.annee = annee;
    }

    @Column(length = Validateurs.MAX_NOMEDITEUR, nullable = false)
    public String getNomEditeur() {
        return nomEditeur;
    }

    public void setNomEditeur(String nomEditeur) {
        this.nomEditeur = nomEditeur;
    }

    @Column(length = Validateurs.MAX_PRENOMAUTEUR, nullable = false)
    public String getPrenomAuteur() {
        return prenomAuteur;
    }

    public void setPrenomAuteur(String prenomAuteur) {
        this.prenomAuteur = prenomAuteur;
    }

    @Column(length = Validateurs.MAX_NOMAUTEUR, nullable = false)
    public String getNomAuteur() {
        return nomAuteur;
    }

    public void setNomAuteur(String nomAuteur) {
        this.nomAuteur = nomAuteur;
    }

    @Column(nullable = false)
    public boolean getEstEmprunte() {
        return estEmprunte;
    }

    public void setEstEmprunte(boolean estEmprunte) {
        this.estEmprunte = estEmprunte;
    }

    @Override
    public String toString() {
        return " Livre n°" + idLivre + " titre=" + titre + " estEmprunte=" + estEmprunte;

    }
}
